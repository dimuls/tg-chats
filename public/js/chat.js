$(function() {

  moment.locale('ru');

  ko.bindingHandlers.autosize = {
    init: function(element, valueAccessor) {
        ko.utils.domNodeDisposal.addDisposeCallback(element, function() {
          var e = document.createEvent('Event');
          e.initEvent('autosize:destroy', true, false);
          element.dispatchEvent(e);
        });
    },
    update: function(element, valueAccessor, allBindings, viewModel, bindingContext) {
      var isAutosizeEnabled = ko.unwrap(valueAccessor());
      if( isAutosizeEnabled ) {
        autosize(element);
      } else {
        var e = document.createEvent('Event');
        e.initEvent('autosize:destroy', true, false);
        element.dispatchEvent(e);
      }
    }
  };

  window.app = {

    ChatConnection: function(opt) {
      var self = this;
      var connecting = false;
      self.connected = ko.observable(false);
      self.wsLocation = (window.location.protocol == "https:" ? "wss://" : "ws://")
                      + "//"
                      + window.location.host
                      + window.location.pathname;
      self.ws = null;
      self.connect = function() {
        connecting = true;
        self.ws = new WebSocket(self.wsLocation);
        self.ws.onerror = function() {
          connecting = false;
          self.connected(false);
          self.ws = null;
        };
        self.ws.onclose = function() {
          connecting = false;
          self.connected(false);
          self.ws = null
        };
        // self.ws.onopen = function() { };
        self.ws.onmessage = function(ev) {
          var ce = JSON.parse(ev.data);
          switch( ce.type ) {
            case "message":
              opt.onMessage(ce.data);
              break;
            case "user in":
              opt.onUserIn();
              break;
            case "user out":
              opt.onUserOut();
              break;
            case "init":
              opt.onInit(ce.data);
              self.connected(true);
              connecting = false;
              break;
          }
        };
      }
      self.send = function(msg) {
        self.ws.send(JSON.stringify(msg));
      }
      setInterval(function() {
        if( self.connected() || connecting ) { return }
        self.connect();
      }, 1000);
    },

    ChatInfo: function(chat) {
      var self = this;
      self.usersCount = ko.observable(1);
      self.userIn = function() {
        self.usersCount(self.usersCount() + 1);
      }
      self.userOut = function() {
        self.usersCount(self.usersCount() - 1);
      }
      self.update = function(data) {
        self.usersCount(data.usersCount);
      }
    },

    ChatInput: function(chat) {
      var self = this;
      self.text = ko.observable()
      self.writing = ko.observable(false)
      self.write = function() {
        if( self.writing() ) { return }
        var text = self.text()
        if( !text.match(/\S/) || text.length > 5000 ) { return }
        self.writing(true);
        chat.write(self.text());
        self.text('');
        self.writing(false);
      }
      self.handleShortcuts = function(data, e) {
        if(e.keyCode == 13 && (e.metaKey || e.ctrlKey)) {
          e.preventDefault()
          self.write();
        }
        return true
      }
    },

    ChatMessage: function(m) {
      var self = this;
      self.header = moment.unix(m.ts).format('D MMM YYYY / HH:MM:ss');
      self.text = m.text;
    },

    Chat: function(opt) {
      var self = this;
      self.room = opt.room;
      self.info = new app.ChatInfo(self);
      self.input = new app.ChatInput(self);
      self.messages = ko.mapping.fromJS([], {
        create: function(o) {
          return new app.ChatMessage(o.data);
        }
      });
      self.isNoMessages = ko.computed(function() {
        return self.messages().length == 0
      });
      self.write = function(text) {
        self.connection.send({
          type: "message", data: text
        })
      }
      self.connected = ko.observable(false);
      self.connection = new app.ChatConnection({
        onInit: function(state) {
          self.info.update(state.info);
          self.messages(state.messages);
        },
        onUserIn: self.info.userIn,
        onUserOut: self.info.userOut,
        onMessage: function(msg) {
          self.messages.unshift(new app.ChatMessage(msg));
          if( self.messages().length > 100 ) {
            self.messages.pop();
          }
        }
      });
    }
  }

  app.chat = new app.Chat($("#chat").data())
  ko.applyBindings(app.chat, document.getElementById('chat-room'));
});
