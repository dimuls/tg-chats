package controllers

import (
	"github.com/frustra/bbcode"
	"github.com/nats-io/nats"
	"github.com/revel/revel"
	"golang.org/x/net/websocket"
	"io"
	"regexp"
	"time"
)

type Event struct {
	Type string      `json:"type"`
	Data interface{} `json:"data"`
}

type Chat struct {
	*revel.Controller
}

func (c Chat) Index() revel.Result {
	return c.Redirect(Chat.Help)
}

func (c Chat) Help() revel.Result {
	// TODO
	return c.Render()
}

func (c Chat) Room(room string) revel.Result {
	c.Validation.Required(room)
	c.Validation.Match(room, regexp.MustCompile("^\\w{1,20}$"))
	if c.Validation.HasErrors() {
		c.Flash.Error("Неправильное имя комнаты. Почитай справку.")
		return c.Redirect(Chat.Help)
	}
	return c.Render(room)
}

func (c Chat) Ws(room string, ws *websocket.Conn) revel.Result {
	roomChannel := "tg-chats/" + room
	revel.INFO.Printf("New websocket connection to room \"%s\".", room)

	nc, err := nats.Connect(nats.DefaultURL)
	if err != nil {
		revel.INFO.Printf("NATS connection error: \"%s\" in room \"%s\". Disconnecting client.", err, room)
		ws.Close()
		return nil
	}

	ec, err := nats.NewEncodedConn(nc, nats.JSON_ENCODER)
	if err != nil {
		revel.INFO.Printf("NATS JSON encoder error: \"%s\" in room \"%s\". Disconnecting client.", err, room)
		ws.Close()
		return nil
	}
	defer ec.Close()

	ec.Publish(roomChannel, Event{
		Type: "user in",
	})

	var clientDisconnected chan bool

	ec.Subscribe(roomChannel, func(e *Event) {
		err := websocket.JSON.Send(ws, e)
		if err != nil {
			revel.INFO.Printf("JSON send error: \"%s\" in room \"%s\". Disconnecting client.", err, room)
			clientDisconnected <- true
		}
	})

	go func() {
		var e Event
		for {
			err := websocket.JSON.Receive(ws, &e)
			if err != nil {
				if err != io.EOF {
					revel.INFO.Printf("JSON receive error: \"%s\" in room \"%s\". Disconnecting client.", err, room)
				}
				clientDisconnected <- true
				return
			}
			text, ok := e.Data.(string)
			if !ok {
				continue
			}
			e.Type = "message"
			e.Data = map[string]interface{}{
				// TODO: id, uid
				"ts":   int(time.Now().Unix()),
				"text": bbcode.NewCompiler(false, false).Compile(text),
			}
			ec.Publish(roomChannel, e)
		}
	}()

	// TODO: store and get real chat messages and info
	err = websocket.JSON.Send(ws, Event{
		Type: "init",
		Data: map[string]interface{}{
			"messages": make([]interface{}, 0),
			"info": map[string]interface{}{
				"usersCount": 1,
			},
		},
	})
	if err != nil {
		revel.INFO.Printf("JSON init error: \"%s\" in room \"%s\". Disconnecting client.", err, room)
		clientDisconnected <- true
	}

	<-clientDisconnected

	ec.Publish(roomChannel, Event{
		Type: "user out",
	})

	return nil
}
